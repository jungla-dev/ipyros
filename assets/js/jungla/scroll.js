window.onscroll = function (e) { 
    e.preventDefault(); 

    let scroll = this.scrollY;
    textAnimation(scroll)
  }

  


  function textAnimation (scroll){
    const leftRight =  document.querySelector('#left-right')
    const rightLeft =  document.querySelector('#right-left')

    if(leftRight && rightLeft){

      leftRight.style.transform= `translate(calc(-80% + ${scroll}px))`
      rightLeft.style.transform= `translate(calc(-20% - ${scroll}px))`
    }else {
      return 
    }
    

  }