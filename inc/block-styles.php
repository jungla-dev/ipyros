<?php
/**
 * Block Styles
 *
 * @link https://developer.wordpress.org/reference/functions/register_block_style/
 *
 * @package WordPress
 * @subpackage IPYROS
 * @since IPYROS 1.0
 */

if ( function_exists( 'register_block_style' ) ) {
	/**
	 * Register block styles.
	 *
	 * @since IPYROS 1.0
	 *
	 * @return void
	 */
	function ipyros_register_block_styles() {
		// Columns: Overlap.
		register_block_style(
			'core/columns',
			array(
				'name'  => 'ipyros-columns-overlap',
				'label' => esc_html__( 'Overlap', 'ipyros' ),
			)
		);

		// Cover: Borders.
		register_block_style(
			'core/cover',
			array(
				'name'  => 'ipyros-border',
				'label' => esc_html__( 'Borders', 'ipyros' ),
			)
		);

		// Group: Borders.
		register_block_style(
			'core/group',
			array(
				'name'  => 'ipyros-border',
				'label' => esc_html__( 'Borders', 'ipyros' ),
			)
		);

		// Image: Borders.
		register_block_style(
			'core/image',
			array(
				'name'  => 'ipyros-border',
				'label' => esc_html__( 'Borders', 'ipyros' ),
			)
		);

		// Image: Frame.
		register_block_style(
			'core/image',
			array(
				'name'  => 'ipyros-image-frame',
				'label' => esc_html__( 'Frame', 'ipyros' ),
			)
		);

		// Latest Posts: Dividers.
		register_block_style(
			'core/latest-posts',
			array(
				'name'  => 'ipyros-latest-posts-dividers',
				'label' => esc_html__( 'Dividers', 'ipyros' ),
			)
		);

		// Latest Posts: Borders.
		register_block_style(
			'core/latest-posts',
			array(
				'name'  => 'ipyros-latest-posts-borders',
				'label' => esc_html__( 'Borders', 'ipyros' ),
			)
		);

		// Media & Text: Borders.
		register_block_style(
			'core/media-text',
			array(
				'name'  => 'ipyros-border',
				'label' => esc_html__( 'Borders', 'ipyros' ),
			)
		);

		// Separator: Thick.
		register_block_style(
			'core/separator',
			array(
				'name'  => 'ipyros-separator-thick',
				'label' => esc_html__( 'Thick', 'ipyros' ),
			)
		);

		// Social icons: Dark gray color.
		register_block_style(
			'core/social-links',
			array(
				'name'  => 'ipyros-social-icons-color',
				'label' => esc_html__( 'Dark gray', 'ipyros' ),
			)
		);
	}
	add_action( 'init', 'ipyros_register_block_styles' );
}
