<?php
/**
 * Customize API: IPYROS_Customize_Notice_Control class
 *
 * @package WordPress
 * @subpackage IPYROS
 * @since IPYROS 1.0
 */

/**
 * Customize Notice Control class.
 *
 * @since IPYROS 1.0
 *
 * @see WP_Customize_Control
 */
class IPYROS_Customize_Notice_Control extends WP_Customize_Control {
	/**
	 * The control type.
	 *
	 * @since IPYROS 1.0
	 *
	 * @var string
	 */
	public $type = 'ipyros-notice';

	/**
	 * Renders the control content.
	 *
	 * This simply prints the notice we need.
	 *
	 * @since IPYROS 1.0
	 *
	 * @return void
	 */
	public function render_content() {
		?>
		<div class="notice notice-warning">
			<p><?php esc_html_e( 'To access the Dark Mode settings, select a light background color.', 'ipyros' ); ?></p>
			<p><a href="<?php echo esc_url( __( 'https://wordpress.org/support/article/ipyros/#dark-mode-support', 'ipyros' ) ); ?>">
				<?php esc_html_e( 'Learn more about Dark Mode.', 'ipyros' ); ?>
			</a></p>
		</div>
		<?php
	}
}
